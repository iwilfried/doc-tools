# Aufgaben und Übungen

Hier sind detaillierte Aufgaben und Projekttätigkeiten für jede Woche, die darauf abzielen, die festgelegten Lernziele zu erreichen:

**Woche 1: Projektgrundlagen und Projektplanung**

* **Aufgabe 1:** Erstellen Sie eine kurze Präsentation, die „Projekt“ und „Projektmanagement“ definiert und diskutieren Sie diese in Kleingruppen.
* **Aufgabe 2:** Entwickeln Sie in Teams SMART-Ziele für das simulierte Kundenschulungsprojekt.
* **Projekttätigkeit:** Erstellen Sie einen grundlegenden Projektstrukturplan und identifizieren Sie die benötigten Ressourcen und potenzielle Risiken.

**Woche 2: Projektorganisation und Methodik**

* **Aufgabe 1:** Weisen Sie in Ihrem Team Rollen zu und definieren Sie die Verantwortlichkeiten für die Kundenschulung.
* **Aufgabe 2:** Wählen und begründen Sie geeignete Projektmanagement-Werkzeuge für Planung und Controlling.
* **Projekttätigkeit:** Entwerfen Sie einen Kommunikationsplan und ein Risikomanagementkonzept für das Projekt.

**Woche 3: Agile und klassische Projektmanagementmethoden**

* **Aufgabe 1:** Erstellen Sie einen Vergleich zwischen agilen und klassischen Projektmanagementmethoden und wählen Sie eine passende Methode für Ihr Schulungsprojekt aus.
* **Aufgabe 2:** Passen Sie Ihren Projektablaufplan anhand des gewählten Projektmanagementansatzes an.
* **Projekttätigkeit:** Planen Sie eine Schulungssitzung, indem Sie agile Techniken wie Scrum oder Kanban einsetzen.

**Woche 4: Durchführung und Kontrolle**

* **Aufgabe 1:** Führen Sie eine simulierte Schulungssitzung durch und sammeln Sie Feedback von den „Teilnehmern“.
* **Aufgabe 2:** Wenden Sie Qualitätsmanagementmethoden an, um Sollwerte mit Istwerten zu vergleichen und Maßnahmen zur Qualitätsverbesserung zu identifizieren.
* **Projekttätigkeit:** Führen Sie eine Bedarfsanalyse durch und erstellen Sie einen Beratungsplan für die Kundenschulung.

**Woche 5: Abschluss und Bewertung**

* **Aufgabe 1:** Erstellen Sie einen detaillierten Abschlussbericht, der die Projektergebnisse, Herausforderungen und Lernerfahrungen dokumentiert.
* **Aufgabe 2:** Entwickeln und halten Sie eine Abschlusspräsentation, die die Schlüsselaspekte und Ergebnisse des Projekts zusammenfasst.
* **Projekttätigkeit:** Führen Sie Feedbackgespräche mit den „Kunden“ und „Teilnehmern“ und werten Sie diese aus, um eine Modul- und Teilnehmerbeurteilung zu erstellen.

Jede dieser Aufgaben und Projekttätigkeiten sollte durch entsprechende Dokumentation unterstützt werden. Die Studierenden sollten **regelmäßig ihre Arbeitsergebnisse und Fortschritte in Form von Berichten, Präsentationen und Reflexionen festhalten.** Feedback-Sitzungen mit den Lehrenden können helfen, die Qualität und Ausrichtung der Projektergebnisse sicherzustellen und den Lernprozess zu vertiefen.
