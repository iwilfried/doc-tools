---
description: zum Weiterlernen
---

# Erklärungen und Links

Projekt ist ein Vorhaben, das durch Einmaligkeit der Bedingungen gekennzeichnet ist. Es beinhaltet verschiedene Aspekte wie Zielvorgaben und wird im Wesentlichen durch seine Einzigartigkeit definiert.

1. Die Unterscheidung zwischen Projekt und Routineaufgaben zeigt, dass bei einem Projekt verschiedene Dinge geplant und festgelegt werden müssen.
2. Es ist wichtig, Projekte von anderen Vorhaben abzugrenzen und verschiedene Arten und Kategorien von Projekten zu unterscheiden.
3. Eine Projektdefinition und die Frage, was Projekte von anderen Vorhaben unterscheidet, sind wichtige Aspekte im Projektmanagement.
4. Ein Projekt ist ein einmaliges Vorhaben, das durch eine temporäre Projektorganisation gemanagt wird und am Ende ein festgelegtes Werk hervorbringt.



Um weitere Informationen zum Thema Projekte und Projektmanagement zu recherchieren, können Sie die unten genannten Quellen besuchen.

{% embed url="https://www.capterra.com.de/blog/772/was-ist-ein-projekt" %}

{% embed url="https://mediencommunity.de/system/files/wbts/projektmanagement/le01/index.html" %}

{% embed url="https://www.youtube.com/watch?v=mrWYpCw7TLI" %}

{% embed url="https://www.projektmagazin.de/glossarterm/projekt" %}

## Flashcards Erstellung

**Flashcard 1:**&#x20;

FRAGE: Was ist ein Projekt?&#x20;

ANTWORT: Ein Projekt ist ein Vorhaben, das sich durch die Einmaligkeit der Bedingungen und Umsetzung auszeichnet.&#x20;

**Flashcard 2:**&#x20;

<mark style="color:blue;">FRAGE:</mark> Was ist der Unterschied zwischen Projekten und Routinearbeiten im Bereich Projektmanagement?&#x20;

<mark style="color:green;">ANTWORT:</mark> Der Unterschied zwischen Projekten und Routinearbeiten besteht darin, dass Projekte verschiedene Dinge planen und festlegen müssen, während Routinearbeiten nicht als Projekt definiert ‚werden, auch wenn sie sehr aufwändig sind.&#x20;

**Flashcard 3:**&#x20;

<mark style="color:blue;">FRAGE:</mark> Was kennzeichnet ein Projekt im Wesentlichen?&#x20;

<mark style="color:green;">ANTWORT:</mark> Ein Projekt ist im Wesentlichen durch die Einmaligkeit der Bedingungen in ihrer Gesamtheit gekennzeichnet, wie zum Beispiel Zielvorgaben.&#x20;

**Flashcard 4:**&#x20;

Nach der Deutschen Industrie Norm (DIN) 69901 wird ein Projekt als ein zielgerichtetes Vorhabe verstanden, das unter Berücksichtigung aller Bedingungen betrachtet wird

&#x20;**Flashcard 5:**&#x20;

<mark style="color:blue;">FRAGE:</mark> Wie kann zwischen "im Projekt", "in Projekten” und "im Projektmanagement” unterschieden werden? Generell kann zwischen "im Projekt”,













