---
description: 'Module 8: Projektmanagemeent (180 UE)'
coverY: 0
---

# Projektmanagement / Curriculum

## Curriculum

<table><thead><tr><th width="239">Lernziel</th><th>Mögliche Inhalte</th></tr></thead><tbody><tr><td>Lernziel 8.01</td><td>Der / Die Umschüler:in erhält eine Einführung in das Projektmanagement.</td></tr><tr><td>Mögliche Inhalte</td><td>Was ist ein Projekt? | Definition Projekt | Definition Projektmanagement | SMART | Projektstrukturen | Arten Projektmanagement | Projektphasen| Projektorganisationen | Funktionen im Team | Magisches Dreieck</td></tr><tr><td>Lernziel 8.02</td><td>Der / Die Umschüler:in kann Projekte selbstständig definieren sowie planen und durchführen.</td></tr><tr><td>Mögliche Inhalte</td><td>Ideen | Struktur (z. B. Projektstrukturplan, Productbacklog, usw.) | Ziele | Methoden | Herangehensweisen | Einschränkungen | Ressourcen | Organisationsstrukturen | Controlling | Werkzeuge (z. B. MS Project) | Kommunikations- und Risikomanagement | Projektphasen | Teammanagement | Rollenverteilung | Stakeholderanalyse</td></tr><tr><td>Lernziel 8.03</td><td>Der / Die Umschüler:in kennt verschiedene Formen des Projektmanagements und kann deren Methoden einordnen sowie anwenden, Arbeitsabläufe reflektieren und Anpassungen vornehmen.</td></tr><tr><td>Mögliche Inhalte</td><td>Klassisch | Agil (z. B. Scrum) | Hybrid | Organisationskultur | Deadlines (z. B. Meilensteine) | Anpassungen bei Über- oder Unterforderung | Prozessmanagement | PRINCE 2 | Lean Management | Organisationsstruktur</td></tr><tr><td>Lernziel 8.04</td><td>Der / Die Umschüler:in kann in Zusammenarbeit mit Kund:innen eine Anforderungsanalyse durchführen, ein situationsgerechtes Kund:innengespräch führen sowie eben diese unter Berücksichtigung ihrer Interessen beraten.</td></tr><tr><td>Mögliche Inhalte</td><td>Projektziele | Anforderungen | gewünschte Ergebnisse | Schulungsbedarfe | Rahmenbedingungen | Planung |Kalkulation | personelle und technische Ressourcen | Kommunikation | Bedarfsanalyse | Beratung | Nutzwertanalyse</td></tr><tr><td>Lernziel 8.05</td><td>Der / Die Umschüler:in kann das Projektergebnis hinsichtlich Zielerreichung, Wirtschaftlichkeit, Skalierbarkeit und Verlässlichkeit bewerten.</td></tr><tr><td>Mögliche Inhalte</td><td>Kriterien | rechtliche Regelungen | betriebliche Grundsätze | Gestaltung von Verträgen | Abwägung von Ressourcen | Pflichten- und Lastenheft | Dokumentation | Auswertung | Abschlussbericht</td></tr><tr><td>Lernziel 8.06</td><td>Der / Die Umschüler:in analysiert und bewertet einen Kundenauftrag und wendet Grundsätze und Methoden des Projektmanagements eigenständig an.</td></tr><tr><td>Mögliche Inhalte</td><td>Kundengespräch | Projektziele | Sachziele | Zeitziele | Qualitätsziele | Kostenziel</td></tr><tr><td>Lernziel 8.07</td><td>Der / Die Umschüler:in prüft und analysiert die Auftragsunterlagen insbesondere in Hinblick aufrechtliche, wirtschaftliche und terminliche Vorgaben.</td></tr><tr><td>Mögliche Inhalte</td><td>Projektbudget | Risikomanagement | Zeitplanung | Personalverfügbarkeit</td></tr><tr><td>Lernziel 8.08</td><td>Der / Die Umschüler:in plant und prüft den Zeitplan und Reihenfolge der Arbeitsschritte für den eigenen Arbeitsbereich.</td></tr><tr><td>Mögliche Inhalte</td><td>Zeitplanung | Projektablaufplan | Gantt Diagramm | Meilensteine | agile Methoden | Netzplantechnik</td></tr><tr><td>Lernziel 8.09</td><td>Der / Die Umschüler:in plant und prüft Qualitätssicherungssysteme im eigenen Arbeitsbereich und wendet diese Qualitätssicherungsmaßnahmen projektbegleitend an und dokumentiert diese.</td></tr><tr><td>Mögliche Inhalte</td><td>Qualitätspolitik | Qualitätsziele | Kennzahlen | Sollwerte | Qualitätsmanagement</td></tr><tr><td>Lernziel 8.10</td><td>Der / Die Umschüler:in interpretiert und bereitet Daten und Sachverhalte multimedial und situationsgerecht unter Nutzung digitaler Werkzeuge und unter Berücksichtigung betrieblicher Vorgaben auf und präsentiert diese vor dem Kunden</td></tr><tr><td>Mögliche Inhalte</td><td>Situationsanalyse | Präsentation erzeugen | Präsentation halten</td></tr><tr><td>Lernziel 8.11</td><td>Modulauswertung | Berücksichtigung teilnehmerbezogener Belange</td></tr><tr><td>Mögliche Inhalte</td><td>Wiederholung der wichtigsten Themen des Moduls | Thematische Exkurse aufgrund von Teilnehmerinteressen Erstellung der Modulbeurteilung und Teilnehmerbeurteilung | Feedbackgespräche</td></tr></tbody></table>

<mark style="color:blue;">IHK-Projekt</mark> und optional <mark style="color:blue;">Herstellerzertifikat</mark> (z. B. **EXIN**, **CompTIA Project+**)

**EXIN** ist eine Organisation, die Zertifizierungen im Bereich Informationstechnologie (IT) anbietet. Als Student für Fachinformatik Anwendungsentwickler könnten EXIN-Zertifizierungen für dich von Interesse sein, um dein Wissen und deine Fähigkeiten in diesem Bereich zu verbessern und dich auf deine Prüfung vorzubereiten. EXIN bietet verschiedene Zertifizierungen an, die sich auf Themen wie IT-Grundlagen, Softwareentwicklung, Projektmanagement und mehr konzentrieren.\
\
Das **CompTIA Project+** Zertifikat ist ein Zertifizierungsexamen, das sich auf Projektmanagement-Fähigkeiten und -Kenntnisse konzentriert. Es ist speziell für Fachleute in der IT-Branche entwickelt, die Projekte leiten oder daran beteiligt sind.



u-FIAE\_NRW Konzeption

26.06.22023 | CM,

25 | 29

{% embed url="https://www.fachinformatiker.de/discover/" %}

{% embed url="https://www.coursera.org/learn/learning-how-to-learn-youth/home/week/1" %}

