# Beispiellektion 45 min

Für eine 45-minütige Unterrichtseinheit in der ersten Woche, die sich auf die Grundlagen des Projektmanagements konzentriert, können Sie den Fokus auf die Definition und die grundlegenden Merkmale eines Projekts legen. Hier ist ein detailliertes Beispiel für eine solche Lektion:

**Thema der Lektion:** Was ist ein Projekt? – Einführung in Projektdefinition und Merkmale

**Lernziele:**

* Die Studierenden können den Begriff „Projekt“ definieren.
* Die Studierenden erkennen die Charakteristika, die ein Vorhaben zu einem Projekt machen.

**Lektionsablauf:**

**Einstieg (5 Minuten):**

* Begrüßung und kurze Vorstellung des Themas.
* Eine einfache Frage stellen: "Was war das letzte Projekt, an dem ihr gearbeitet habt?", um die Studierenden zum Nachdenken anzuregen und ein erstes Verständnis für die Vielfalt von Projekten zu entwickeln.

**Präsentation und Diskussion (15 Minuten):**

* Kurze Präsentation der Definition eines Projekts mit Beispielen aus verschiedenen Branchen.
* Diskussion über die Merkmale eines Projekts (zeitlich befristet, einzigartig, zielorientiert, etc.).

**Gruppenarbeit (10 Minuten):**

* Die Studierenden werden in Kleingruppen eingeteilt.
* Jede Gruppe erhält die Aufgabe, ein reales Beispiel für ein Projekt zu finden und zu diskutieren, warum es als Projekt gilt (anhand der zuvor besprochenen Merkmale).

**Ergebnispräsentation und Feedback (10 Minuten):**

* Jede Gruppe präsentiert ihr Beispielprojekt und ihre Begründung.
* Kurzes Feedback durch den Dozenten und die anderen Studierenden.

**Abschluss und Reflexion (5 Minuten):**

* Zusammenfassung der Kernpunkte durch den Dozenten.
* Reflexionsfrage an die Studierenden, wie das Gelernte auf ihre eigenen Erfahrungen anwendbar ist.

**Hausaufgabe / Selbststudium (Nach der Lektion):**

* Die Studierenden sollen eine kurze Beschreibung eines Projekts aus ihrem Erfahrungsbereich verfassen und dabei die besprochenen Charakteristika verwenden.

Diese Unterrichtseinheit ist darauf ausgelegt, die Studierenden aktiv zu beteiligen und ihnen zu helfen, das Konzept eines Projekts durch Beispiele und Diskussionen zu verstehen. Die Gruppenarbeit fördert die Zusammenarbeit und das Feedback sorgt für ein vertieftes Verständnis. Die Hausaufgabe dient dazu, das Gelernte zu festigen und auf die eigene Erfahrung zu beziehen.
