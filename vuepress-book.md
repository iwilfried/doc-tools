---
description: 'Vorgehensmodell: Umsetzung des Curriculums'
coverY: 0
---

# Selbstgesteuertes Lernen

<figure><img src=".gitbook/assets/bildprojekt.jpg" alt=""><figcaption></figcaption></figure>

Für einen 25-tägigen Plan zum selbstgesteuerten Lernen im Bereich Projektmanagement für Fachinformatiker mit insgesamt 180 Unterrichtseinheiten (UE) könnte das vorgegebene Curriculum wie folgt strukturiert sein:

## Planstruktur:

* Tägliche Struktur: Jeder Tag besteht aus 7,2 UE (angenommen ein Tag hat 8 Stunden, werden 10% Pufferzeit für Pausen und Unvorhergesehenes eingeplant).
* Wochenstruktur: 5 Tage pro Woche, Montag bis Freitag, mit Wochenenden zur Selbstreflexion und Vor- bzw. Nachbereitung.
* Selbstlernphasen: werden im Plan festgehalten und mit entsprechenden Ressourcen unterstützt.
* Praktische Phasen: Beinhalten die Anwendung des Gelernten in Projekten oder Übungen.
* Feedback und Reflexion: Finden regelmäßig statt, um den Fortschritt zu überwachen und das Verständnis zu vertiefen.

## Modulplan:

#### **Tag 1-3: Einführung in das Projektmanagement (Lernziel 8.01)**

* Inhalte: Grundbegriffe, SMART-Ziele, Projektstrukturen.
* Methodik: YouTube, Artikel, Diskussionsforen, Zusammenfassung, Notion
* UE: 21,6

#### **Tag 4-7: Projekte definieren, planen und durchführen (Lernziel 8.02)**

* Inhalte: Ideenentwicklung, Zielsetzung, Methodik.
* Methodik: Erstellung eines Projektstrukturplans, Einführung in MS Project.
* UE: 28,8

#### **Tag 8-10: Formen des Projektmanagements (Lernziel 8.03)**

* Inhalte: Klassisches, agiles, hybrides Projektmanagement.
* Methodik: Case Studies, Gruppenarbeit, Scrum-Simulation.
* UE: 21,6

#### **Tag 11-13: Zusammenarbeit mit Kund:innen (Lernziel 8.04)**

* Inhalte: Anforderungsanalyse, Kundengespräche.
* Methodik: Rollenspiele, Kund:innengespräche simulieren.
* UE: 21,6

#### **Tag 14-16: Bewertung des Projektergebnisses (Lernziel 8.05)**

* Inhalte: Kriterien der Bewertung, Vertragsmanagement.
* Methodik: Erstellen von Kriterienkatalogen, Diskussionen.
* UE: 21,6

#### **Tag 17-18: Analyse und Anwendung von Projektmanagement (Lernziel 8.06)**

* Inhalte: Analyse von Projektzielen, Anwendung von Grundsätzen.
* Methodik: Projektarbeit, Selbststudium.
* UE: 14,4

#### **Tag 19: Rechtliche, wirtschaftliche und terminliche Prüfung (Lernziel 8.07)**

* Inhalte: Projektbudget, Risikomanagement.
* Methodik: Case Studies, Expertengastvorträge.
* UE: 7,2

#### **Tag 20-21: Zeitplanung und Arbeitsablauf (Lernziel 8.08)**

* Inhalte: Zeitmanagement, Gantt-Diagramm.
* Methodik: Erstellen von Zeitplänen, Software-Training.
* UE: 14,4

#### **Tag 22: Qualitätssicherung (Lernziel 8.09)**

* Inhalte: Qualitätspolitik, Qualitätsmanagement.
* Methodik: Workshops, Erstellen von Qualitätsplänen.
* UE: 7,2

#### **Tag 23: Dateninterpretation und Präsentation (Lernziel 8.10)**

* Inhalte: Multimediale Aufbereitung, Präsentationstechniken.
* Methodik: Präsentationsübungen, Feedbacksessions.
* UE: 7,2

#### **Tag 24: Modulauswertung und Feedback (Lernziel 8.11)**

* Inhalte: Thematische Exkurse, Modulbeurteilung.
* Methodik: Feedbackrunden, Selbst- und Gruppenbewertungen.
* UE: 7,2

**Tag 25: Abschluss und Präsentation**

* Inhalte: Zusammenfassung des Gelernten, Präsentation der Projektergebnisse.
* Methodik: Abschlusspräsentation



## Lerneinheiten

***

Tag 1-3:&#x20;

**Einführung in das Projektmanagement (Lernziel 8.01)**

`Lerneinheit  1:`&#x20;

* Was ist ein Projekt?
* Erläuterung des Projektbegriffs
* Unterscheidung von Projekten und Routinetätigkeiten
* Beispiele für Projekte in der IT

`Lerneinheit 2:`&#x20;

* Definition Projekt
* Definition und Charakteristika eines Projekts
* Lebenszyklus eines Projekts

`Lerneinheit 3:`&#x20;

* Definition Projektmanagement
* Definition des Begriffs Projektmanagement
* Ziele und Nutzen des Projektmanagements
* Rolle des Projektmanagements in Organisationen

`Lerneinheit 4:`&#x20;

* SMART
* Erklärung der SMART-Kriterien
* Anwendung der SMART-Formel auf Projektziele
* Übung: Formulieren von SMART-Zielen für ein Beispielprojekt

`Lerneinheit 5:`&#x20;

* Projektstrukturen
* Aufbauorganisation von Projekten
* Unterschiede zwischen funktionalen, divisionalen und Matrix-Strukturen
* Rollen und Verantwortlichkeiten in Projektstrukturen

`Lerneinheit 6:`&#x20;

* Arten von Projektmanagement
* Vorstellung verschiedener Projektmanagement-Ansätze (klassisch, agil, hybrid)
* Diskussion der Vor- und Nachteile jeder Methode
* Kontext, in dem jede Methode am besten geeignet ist

`Lerneinheit 7:`&#x20;

* Projektphasen
* Detaillierte Beschreibung der einzelnen Projektphasen (Initiierung, Planung, Ausführung, Kontrolle und Abschluss)
* Bedeutung und Aktivitäten in jeder Phase

`Lerneinheit 8:`&#x20;

* Projektorganisationen
* Typen von Projektorganisationen (Projektbüro, PMO etc.)
* Funktion und Einfluss von Projektorganisationen auf Projekterfolg

`Lerneinheit 9:`&#x20;

* Funktionen im Team
* Verschiedene Teamrollen und deren Beiträge zum Projekt
* Teambildung und Entwicklung von High-Performance-Teams

`Lerneinheit 10:`&#x20;

* Magisches Dreieck
* Konzept des magischen Dreiecks (Zeit, Kosten, Umfang)
* Balancieren der drei Aspekte und Umgang mit Trade-offs

### **Methodik und Ressourcen:**

{% hint style="info" %}
* Selbstlernmaterialien: Lehrvideos, Fachartikel, E-Books, Google
* Interaktive Elemente: Diskussionsforen, virtuelle Klassenzimmer, Flashcards
* Praktische Anwendung: Fallstudien, Szenario-Analysen, Projektarbeit
* Feedback und Support: Regelmäßige Q\&A-Sessions, Peer-Reviews, Tutorien
{% endhint %}



**Zeitplan:**

* Dauer: 3 Tage (entsprechend 21,6 UE)
* Selbststudium: 5 UE täglich
* Praktische Übungen: 2 UE täglich
* Feedback und Reflexion: 0,6 UE täglich

**Bewertung und Feedback:**

* <mark style="color:blue;">Selbstbewertung:</mark> Anhand von Checklisten und Quizzes (Flashcards) das eigene Verständnis überprüfen
* <mark style="color:blue;">Gruppenfeedback:</mark> In Diskussionsforen Erkenntnisse teilen und von anderen lernen
* <mark style="color:blue;">Lehrerfeedback:</mark> Einreichung von Übungsaufgaben zur Bewertung und Kommentierung durch den Lehrer



