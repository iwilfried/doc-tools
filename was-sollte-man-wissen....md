---
description: Modul 8.01
---

# Was sollte man wissen...

Um ein umfassendes Verständnis über die genannten Themen im Bereich Projektmanagement zu erlangen, hier die wichtigsten Punkte, die man wissen sollte, inklusive Empfehlungen, wo man weiterführende Informationen finden kann:

1. **Was ist ein Projekt?**
   * Ein Projekt ist ein zeitlich begrenztes Vorhaben, um ein einzigartiges Produkt, einen Service oder ein Ergebnis zu schaffen.
   * Projekte zeichnen sich durch Einmaligkeit, definierte Anfangs- und Endpunkte, spezifische Ziele und Anforderungen aus.
2. **Definition Projekt**
   * Die offizielle Definition eines Projekts kann z.B. im PMBOK® Guide des Project Management Institute (PMI) gefunden werden.
3. **Definition Projektmanagement**
   * Projektmanagement ist die Anwendung von Wissen, Fähigkeiten, Werkzeugen und Techniken auf Projektaktivitäten, um Projektanforderungen zu erfüllen.
   * Weiterführend: [Project Management Institute (PMI)](https://www.pmi.org/)
4. **SMART**
   * SMART ist ein Akronym, das steht für Specific, Measurable, Achievable, Relevant und Time-bound, und wird verwendet, um Ziele klar und erreichbar zu definieren.
   * Weiterführend: [SMART criteria - Wikipedia](https://en.wikipedia.org/wiki/SMART\_criteria)
5. **Projektstrukturen**
   * Projektstrukturen definieren die Hierarchie und die Beziehungen zwischen den verschiedenen Komponenten und Aktivitäten eines Projekts.
   * Dazu gehören Organisationsstrukturen wie funktionale, projektorientierte oder Matrixstrukturen.
   * Weiterführend: [Types of Project Organizational Structure - PM Study Circle](https://pmstudycircle.com/types-of-project-organizational-structure/)
6. **Arten Projektmanagement**
   * Es gibt verschiedene Arten von Projektmanagement, z.B. klassisch, agil, Lean, PRINCE2, etc.
   * Jede Methode hat eigene Prinzipien und Praktiken.
   * Weiterführend: [Different Types of Project Management Methodologies - Wrike](https://www.wrike.com/project-management-guide/faq/what-are-the-different-types-of-project-management-methodologies/)
7. **Projektphasen**
   * Projektphasen sind die Stufen, die ein Projekt von der Initialisierung bis zum Abschluss durchläuft.
   * Typische Phasen sind Initiierung, Planung, Ausführung, Überwachung/Kontrolle und Abschluss.
   * Weiterführend: [The 5 Project Management Steps To Run Every Project Perfectly - The Digital Project Manager](https://thedigitalprojectmanager.com/project-management-steps/)
8. **Projektorganisationen**
   * Projektorganisationen sind Strukturen, die festlegen, wie Teams zusammengestellt werden und wie die Kommunikation und Hierarchie innerhalb eines Projekts gestaltet ist.
   * Weiterführend: [Organizational Structure Types for Project Managers - ProjectManager](https://www.projectmanager.com/blog/organizational-structure-types-project-managers)
9. **Funktionen im Team**
   * Zu den Funktionen im Projektteam gehören Rollen wie der Projektleiter, Teammitglieder, Stakeholder, Sponsoren und Kunden.
   * Jede Rolle hat spezifische Aufgaben und Verantwortlichkeiten.
   * Weiterführend: [Project Team Roles and Responsibilities - TeamGantt](https://www.teamgantt.com/guide-to-project-management/project-team-roles-and-responsibilities)
10. **Magisches Dreieck**
    * Das magische Dreieck, auch bekannt als Triple Constraint, bezieht sich auf die drei Hauptbeschränkungen eines Projekts: Zeit, Kosten und Umfang (Scope).
    * Die Qualität des Projektergebnisses wird oft als vierte Dimension hinzugefügt.
    * Weiterführend: [The Project Management Triangle - Time, Cost and Quality - PM Tips](https://pmtips.net/article/project-management-triangle-time-cost-quality)

Diese Punkte bieten eine solide Grundlage für das Verständnis der Grundlagen des Projektmanagements. Für ein tieferes Eintauchen empfehlen sich umfassende Kurse, Zertifizierungen oder die Lektüre von Fachbüchern zum Projektmanagement. Webseiten von Projektmanagement-Organisationen wie PMI bieten eine Fülle von Ressourcen, ebenso wie akademische Journale und Publikationen in diesem Bereich.
