# Lernziele pro Woche

Spezifische Lernziele für jede der fünf Wochen:

**Woche 1: Projektgrundlagen und Projektplanung**

* Die Studierenden sollen die Definition eines Projekts verstehen und die Charakteristika eines Projektes benennen können.
* Sie sollen lernen, wie man ein Projekt mithilfe der SMART-Kriterien definiert und Ziele festlegt.
* Die Studierenden sollen verschiedene Projektstrukturen untersuchen und eine passende für die Kundenschulung auswählen.
* Sie sollen einen Projektstrukturplan erstellen und die wichtigsten Projektmanagement-Werkzeuge kennenlernen.

**Woche 2: Projektorganisation und Methodik**

* Die Studierenden sollen verschiedene Projektorganisationen und deren Einfluss auf die Projektarbeit verstehen.
* Sie sollen Funktionen und Rollen innerhalb eines Projektteams definieren und zuordnen können.
* Sie sollen Controlling-Mechanismen und Projektmanagement-Werkzeuge anwenden, um den Fortschritt des Projekts zu planen und zu überwachen.
* Die Studierenden sollen lernen, wie sie mit Herausforderungen und Einschränkungen im Projektmanagement umgehen.

**Woche 3: Agile und klassische Projektmanagementmethoden**

* Die Studierenden sollen die Unterschiede zwischen klassischen und agilen Projektmanagementmethoden verstehen und diese Wissen auf ein Projekt anwenden.
* Sie sollen in der Lage sein, Methoden an die spezifische Projektkultur anzupassen und Meilensteine zu definieren.
* Die Studierenden sollen Projektanpassungen planen und durchführen, um auf Über- oder Unterforderungen zu reagieren.

**Woche 4: Durchführung und Kontrolle**

* Die Studierenden sollen eine Kundenschulung unter Berücksichtigung von Projektzielen und Rahmenbedingungen durchführen können.
* Sie sollen Qualitätsmanagementtechniken einsetzen, um die Qualität der Schulung zu sichern und zu verbessern.
* Die Studierenden sollen lernen, wie sie eine Bedarfsanalyse durchführen und auf Basis dieser eine Beratung formulieren.

**Woche 5: Abschluss und Bewertung**

* Die Studierenden sollen in der Lage sein, eine kritische Auswertung des Projekts durchzuführen und einen Abschlussbericht zu erstellen.
* Sie sollen eine professionelle Präsentation vorbereiten und halten können, die die wichtigsten Ergebnisse des Projekts zusammenfasst.
* Die Studierenden sollen Feedbackgespräche führen und eine Modulbeurteilung sowie eine Teilnehmerbeurteilung erstellen können.

Jedes dieser Lernziele sollte durch entsprechende Aufgaben und Projekttätigkeiten unterstützt werden, um sicherzustellen, dass die Studierenden die Theorie in die Praxis umsetzen und die angestrebten Kompetenzen erreichen.
