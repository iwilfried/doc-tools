# Table of contents

* [Projektmanagement / Curriculum](README.md)
* [Selbstgesteuertes Lernen](vuepress-book.md)
* [Erklärungen und Links](getting-started-with-vuepress.md)
* [Verschiedenes](verschiedenes.md)
* [Alternative Aufgabenstellung](alternative-aufgabenstellung.md)
* [Lernziele pro Woche](lernziele-pro-woche.md)
* [Aufgaben und Übungen](aufgaben-und-uebungen.md)
* [Kundenangebot](kundenangebot.md)
* [Schritt für Schritt Anleitung](schritt-fuer-schritt-anleitung.md)
* [Beispiellektion 45 min](beispiellektion-45-min.md)
* [Was sollte man wissen...](was-sollte-man-wissen....md)
* [Selbst Lernen](selbst-lernen.md)
* [Aktivitäten](aktivitaeten.md)
* [Entwicklertagebuch](entwicklertagebuch.md)
* [Mindmap](mindmap.md)
* [Kalender App](kalender-app.md)
* [Werkzeuge](werkzeuge.md)
